###Pour installer le projet :

`git clone git@gitlab.com:ThibautMartin/fullstackrs.git`

Puis entrer dans le projet `cd fullstackrs`
##
Allez dans le dossier rs_back : `cd rs_back`

Installez tous les packages pour le back : `npm install`

Lancez le serveur back : `npm start`
##
Allez ensuite dans le dossier rs_front : `cd ../rs_front`

Installez tous les packages pour le front : `npm install`

Lancez le serveur front : `npm run start`

---------


## L'application

### Créer un user

Par défaut, le formulaire de connexion s'affichera à l'écran. Au début vous n'avez pas de compte. Suivez la petite indication au dessous du formulaire !

### Se connecter

Après vous êtes inscrit, connectez vous avec les identifiants de connexion que vous avez renseigné.

### Bienvenue sur l'appli

Dans l'AppBar, vous aurez la possibilité de revenir sur la page d'accueil en cliquant sur le nom de l'app.
Vous pouvez également visiter votre profil, vous déconnecter ou encore allez sur la page dédiée au chat (le comportement du chat reste encore approximatif...)

Vous pouvez créer un post, le modifier ou le supprimer.
Commenter ce post.
L'aimer, que ce soit le votre ou non.

Sur votre profil, vous pouvez mettre à jour votre photo. Choisissez la depuis votre galerie puis redimensionnez la !
Vous verrez également l'ensemble de vos posts ainsi que les commentaires qui y sont attachés.

Si vous visitez le profil d'un autre utilisateur, vous pourrez le demander en ami !