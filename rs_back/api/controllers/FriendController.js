'use strict';


var mongoose = require('mongoose'),
    Friend = mongoose.model('Friend');

exports.list = function(req, res) {
    Friend.find({}, function(err, friend) {
        if (err)
            return res.send(err);
        return res.json(friend);
    });
};

exports.read = function(req, res) {
    Friend.findOne({ 'authorId': req.params.authorId, 'recipientId': req.params.recipientId }).exec(function(err, friend) {
            if (err)
                return res.send(err);
            if (friend) {
                return res.json({_id: friend._id, state: friend.state, comment: 'asking'});
            } else {
                Friend.findOne({ 'authorId': req.params.recipientId, 'recipientId': req.params.authorId }).exec(function(err, friend) {
                        if (err)
                            return res.send(err);
                        if (friend) {
                            return res.json({_id: friend._id, state: friend.state, comment: 'asked'});
                        } else {
                            return res.json({state: -1});
                        }
                    }
                );
            }
        }
    );

};

exports.create = function(req, res) {
    var new_friend = new Friend(req.body);
    new_friend.save(function(err, friend) {
        if (err)
            return res.send(err);
        return res.json({_id: friend._id, state: 0, comment: 'asking'});
    });
};

exports.update = function(req, res) {
    Friend.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}).exec(function(err, friend) {
        if (err)
            return res.send(err);
        return res.json({_id: friend._id, state: 1});
    });
};

exports.delete = function(req, res) {
    Friend.remove({_id: req.params.id}, function(err, friend) {
        if (err)
            return res.send(err);
        return res.json({ message: 'Friend successfully deleted !' });
    });
};