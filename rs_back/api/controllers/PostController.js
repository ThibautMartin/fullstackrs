'use strict';


var mongoose = require('mongoose'),
    Post = mongoose.model('Post');

exports.list = function(req, res) {
    Post.find({}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
            if (err)
                return res.send(err);
            return res.json(post);
    });
};

exports.getFromUser = function(req, res) {
    Post.find({author: req.params.userId}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
        if (err)
            return res.send(err);
        return res.json(post);
    });
};

exports.create = function(req, res) {
    var new_post = new Post(req.body).populate('author');
    new_post.save(function(err, post) {
        if (err)
            return res.send(err);
        return res.json(post);
    });
};


exports.read = function(req, res) {
    Post.findOne({ _id: req.params.postId}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
        if (err)
            return res.send(err);
        return res.json(post);
    });
};


exports.update = function(req, res) {
    Post.findOneAndUpdate({_id: req.params.postId}, req.body, {new: true}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
        if (err)
            return res.send(err);
        return res.json(post);
    });
};


exports.updateLike = function(req, res) {
    let newLiker = req.params.userId;
    Post.findOneAndUpdate({_id: req.params.postId}, { $push: {likes: newLiker } }, {new: true}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
        if (err) {
            return res.send(err);
        }
        return res.json(post);
    });
};


exports.updateUnlike = function(req, res) {
    let newUnliker = req.params.userId;
    Post.findOneAndUpdate({_id: req.params.postId}, { $pull: {likes: newUnliker } }, {new: true}).populate('author').populate('comments').populate({
        path: 'comments',
        populate: {
            path: 'author',
            model: 'User'
        }
    }).exec(function(err, post) {
        if (err) {
            return res.send(err);
        }
        return res.json(post);
    });
};


exports.delete = function(req, res) {
    Post.remove({_id: req.params.postId}, function(err, post) {
        if (err)
            return res.send(err);
        return res.json({ message: 'Post successfully deleted !' });
    });
};
