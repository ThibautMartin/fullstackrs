'use strict';


var mongoose = require('mongoose'),
    Room = mongoose.model('Room');

exports.read = function(req, res) {
    let user1 = req.params.authorId;
    let user2 = req.params.recipientId;
    Room.find(
        {
            $or:
                [
                    { 'authorId': user1, 'recipientId': user2 },
                    { 'authorId': user2, 'recipientId': user1 }
                ]
        }
    ).exec(function(err, room) {
            if (room.length === 0) {
                var new_room = new Room({ 'authorId': user1, 'recipientId': user2 });
                new_room.save(function(err) {
                    return !err;
                });
            }
            if (err)
                return res.send(err);
            return res.json(room);
        }
    );
};
