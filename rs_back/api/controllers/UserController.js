'use strict';


var mongoose = require('mongoose'), User = mongoose.model('User');

exports.list = function(req, res) {
    User.find({}, function(err, user) {
        if (err)
            return res.send(err);
        return res.json(user);
    });
};

exports.create = function(req, res) {
    var new_user = new User(req.body);
    new_user.save(function(err, user) {
        if (err)
            return res.send(err);
        return res.json(user);
    });
};


exports.read = function(req, res) {
    User.findOne({ _id: req.params.userId }).exec(function(err, user) {
        if (err)
            return res.send(err);
        return res.json(user);
    });
};


exports.findOne = function(req, res) {
    User.findOne({username: req.params.username}, function(err, user) {
        if (err)
            return res.send(err);
        return res.json(user);
    });
};


exports.update = function(req, res) {
    User.findOneAndUpdate({_id: req.params.userId}, req.body, {new: true}, function(err, user) {
        if (err)
            return res.send(err);
        return res.json(user);
    });
};


exports.delete = function(req, res) {
    User.remove({_id: req.params.userId}, function(err, user) {
        if (err)
            return res.send(err);
        return res.json({ message: 'User successfully deleted !' });
    });
};
