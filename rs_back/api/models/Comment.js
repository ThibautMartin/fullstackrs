'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CommentSchema = new Schema({
    content: {
        type: String,
        required: 'Comment needs content'
    },
    author: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    post: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Post',
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Comment', CommentSchema);