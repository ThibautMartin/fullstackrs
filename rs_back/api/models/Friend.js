'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var FriendSchema = new Schema({
    authorId: {
        type: String
    },
    recipientId: {
        type: String
    },
    state: {
        type: Number,
        default: 0,
    },
});

module.exports = mongoose.model('Friend', FriendSchema);