'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MessageSchema = new Schema({
    authorId: {
        type: String
    },
    recipientId: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now()
    },
    author: {
        type: String
    },
    recipient: {
        type: String
    },
    message: {
        type: String
    },
    room: {
        type: String
    }
});

module.exports = mongoose.model('Message', MessageSchema);