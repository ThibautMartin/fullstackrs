'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
    firstName: {
        type: String,
        required: 'User needs first name'
    },
    lastName: {
        type: String,
        required: 'User needs last name'
    },
    username: {
        type: String,
        required: 'User needs username'
    },
    password: {
        type: String,
        required: 'User needs password'
    },
    photo: {
        type: String,
        default: 'default-profile.png'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('User', UserSchema);