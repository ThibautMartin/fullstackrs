'use strict';
module.exports = function(app) {
    var post = require('../controllers/PostController');

    app.route('/posts')
        .get(post.list)
        .post(post.create);


    app.route('/posts/:postId')
        .get(post.read)
        .put(post.update)
        .delete(post.delete);

    app.route('/posts/like/:postId/:userId')
        .put(post.updateLike);

    app.route('/posts/unlike/:postId/:userId')
        .put(post.updateUnlike);

    app.route('/posts/user/:userId')
        .get(post.getFromUser);
};