var express = require('express'),
    app = express(),
    port = process.env.PORT || 3001,
    mongoose = require('mongoose'),
    User = require('./api/models/User'),
    Post = require('./api/models/Post'),
    Comment = require('./api/models/Comment'),
    Message = require('./api/models/Message'),
    Room = require('./api/models/Room'),
    Friend = require('./api/models/Friend'),
    MessageController = require('./api/controllers/MessageController'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    cors = require('cors'),
    fs = require('fs');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/fullstackrs', { useNewUrlParser: true } );

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(cors());
app.use(express.static('public'));


// Upload profile photo to server
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/profile');
    },
    filename: function (req, file, cb) {
        let ext = file.originalname.split('.')[file.originalname.split('.').length - 1];
        newPhotoName = _uid + '.' + ext;
        cb(null, newPhotoName);
    }
});
var upload = multer({ storage: storage }).single('file');
var _uid = null;
var newPhotoName = null;
app.post('/upload/:uid',function(req, res) {
    _uid = req.param("uid");
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        return res.status(200).send(newPhotoName);
    })
});
app.get('/image/:photoName',function(req, res) {
    let photoName = req.param("photoName");
    var img = fs.readFile('./public/images/profile/' + photoName, function (err, data) {
        var contentType = 'image/png';
        var base64 = Buffer.from(data).toString('base64');
        base64='data:image/png;base64,'+base64;
        return res.send(base64);
    });
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routesUser = require('./api/routes/User');
routesUser(app);
var routesPost = require('./api/routes/Post');
routesPost(app);
var routesComment = require('./api/routes/Comment');
routesComment(app);
var routesMessage = require('./api/routes/Message');
routesMessage(app);
var routesRoom = require('./api/routes/Room');
routesRoom(app);
var routesFriend = require('./api/routes/Friend');
routesFriend(app);


/**
 * Socket.io
 */

server = app.listen(8080);
var socket = require('socket.io'),
    io = socket(server);

io.on('connection', function(socket) {
    socket.leaveAll();
    socket.on('CHANGE_ROOM', function(data) {
        socket.leaveAll();
        socket.join(data);
    });
    socket.on('SEND_MESSAGE', function(data){
        MessageController.create(data);
        io.to(data.room).emit('RECEIVE_MESSAGE', data);
    });
});

app.listen(port);

app.use(function(req, res) {
    return res.status(404).send({url: req.originalUrl + ' not found'})
});
