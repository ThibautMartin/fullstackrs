import React, { Component } from 'react';
import axios from "axios";
import '../login/Login'
import AppBar from "../components/AppBar";
import ProfileContent from "../components/ProfileContent";

const data = {
    post1: {
        username: 'Thibaut Martin',
        date: '17 mars 2019, 08:45',
        url: '/images/cards/fighter.jpg',
    },
}

const styles = theme => ({
    profileImg: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
});

class Profile extends Component {

    state = {
        userData: [],
        email: '',
    };

    constructor(props) {
        super(props);
    }

    redirectToHome = () => {
        this.props.history.push('/')
    }

    updateField = (e) => {
        e.preventDefault();
        document.getElementById('email').removeAttribute('disabled')
        document.getElementById('saveEmail').removeAttribute('hidden')
        document.getElementById('updateEmail').setAttribute('hidden', 'hidden')
    }

    saveField = (e) => {
        e.preventDefault()
        document.getElementById('email').setAttribute('disabled', 'disabled')
        document.getElementById('updateEmail').removeAttribute('hidden')
        document.getElementById('saveEmail').setAttribute('hidden', 'hidden')
        axios.put("http://localhost:3001/users/" + sessionStorage.getItem('token'), "{'email': 'test@@mail}")
            .then(data => {
                    console.log('updated !');
                }
            );
        return fetch('/users/' + this.state.userData._id, {
            method: 'PUT',
            mode: 'CORS',
            //body: JSON.stringify(),
            body: "{'email': 'thibatjjefjeklf'}",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            return res;
        }).catch(err => err);
    }

    handleOnChange = (email) => {
        this.setState({
            email: email,
        });
    }

    componentDidMount() {
        if (!sessionStorage.getItem('token'))
            this.props.history.push('/login');
        this.getUserInfo();
    }

    getUserInfo = () => {
        axios.get("http://localhost:3001/users/" + sessionStorage.getItem('token'))
            .then(data => {
                    this.setState({
                        userData: data['data'],
                        email: data['data'].email,
                    });
                }
            );
    };

    render() {
        const { classes } = this.props;
        const userData = this.state.userData;
        return (
            <div className="Profile">
                <AppBar data={ this.props }/>
                <br/>
                <ProfileContent data={data.post1} props={ this.props }/>
                <br/>
                <button onClick={this.redirectToHome}>Home</button>
                <br />
                <br />
                <div>
                    <div key={userData._id}>{userData.username} {userData.email}</div>
                    {/*<img src={'/images/user/profile/avatar.jpg'} alt="profile avatar" className="img-responsive" />*/}
                    <form id={'updateUserInfo'} >
                        Your email : <input id={'email'} type={'text'} disabled={'disabled'} value={this.state.email} onChange={() => this.handleOnChange(this.value)}/>
                        <button id={'updateEmail'} onClick={this.updateField}>modifier</button>
                        <button id={'saveEmail'} onClick={this.saveField} hidden={'hidden'}>enregistrer</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Profile;
