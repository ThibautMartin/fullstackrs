import React, {Component} from "react";
import { withStyles } from "@material-ui/core";
import { Link } from 'react-router-dom'
import { redirectToLogin } from '../helpers/Redirections';
import CombineStyles from "./CombineStyles";
import GlobalStyle from "./GlobalStyle";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from '@material-ui/icons/AccountCircle';
import ChatRounded from '@material-ui/icons/ChatRounded';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Home from '@material-ui/icons/Home';
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

const LocalStyle = () => ({
    flexbox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
});

class AppBarCustom extends Component {
    state = {
        phoneSize: window.innerWidth <= 600,
    };

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    updateDimensions() {
        this.setState({phoneSize: window.innerWidth <= 600})
    }

    logout = () => {
        sessionStorage.removeItem('token');
        redirectToLogin(this.props.data);
    };

    render() {
        const { classes } = this.props;
        return (
            <AppBar position="static">
                <Toolbar className={classes.flexbox}>
                    <Link className={classes.link} to="/">
                        FullStackRS
                        {
                            this.state.phoneSize &&
                            <IconButton color="inherit">
                                <Home />
                            </IconButton>
                        }
                    </Link>
                    <div>
                        <Link className={classes.link} to="/chat">
                            <IconButton color="inherit">
                                <ChatRounded />
                            </IconButton>
                        </Link>
                        <Link className={classes.link} to="/profile">
                            <IconButton color="inherit">
                                <AccountCircle />
                            </IconButton>
                        </Link>
                        {
                            !this.state.phoneSize &&
                            <Button color="primary" variant="contained" onClick={this.logout}>
                                Disconnection
                            </Button>
                        }
                        {
                            this.state.phoneSize &&
                            <IconButton color="inherit" onClick={this.logout}>
                                <ExitToApp />
                            </IconButton>
                        }
                    </div>
                </Toolbar>
            </AppBar>
        )
    }
}
const combinedStyles = CombineStyles(GlobalStyle, LocalStyle);

export default withStyles(combinedStyles)(AppBarCustom);