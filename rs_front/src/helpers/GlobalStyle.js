const globalStyle = theme => ({
    root: {
        flexGrow: 1,
    },
    paperGlobal: {
        padding: theme.spacing.unit * 2,
    },
    modalPhoto: {
        position: 'absolute',
        width: '90%',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 2,
        outline: 'none',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },
    chipError: {
        margin: theme.spacing.unit,
        color: '#721c24',
        backgroundColor: '#f8d7da',
        border: '2px solid #f5c6cb'
    },
    chipSuccess: {
        margin: theme.spacing.unit,
        color: '#155724',
        backgroundColor: '#d4edda',
        border: '2px solid #c3e6cb'
    },
    centered: {
        textAlign: 'center',
    },
    centeredIntoModal: {
        textAlign: 'center',
        marginBottom: '30px',
    },
    margin: {
        margin: theme.spacing.unit,
    },
    hr: {
        borderTop: '1px solid #e0e0e0',
        marginTop: 10,
        marginBottom: 10,
    },
    title: {
        flexGrow: 1,
        cursor: 'pointer'
    },
    link: {
        textDecoration: 'none',
        color: 'inherit',
    },
    alignToRight: {
        textAlign: 'right',
    },
    fullWidth: {
        width: '100%',
    },
    pointer: {
        cursor: 'pointer',
    }
});

export default globalStyle;