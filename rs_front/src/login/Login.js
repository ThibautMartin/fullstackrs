import React, { Component } from 'react';
import './Login.css';
import CombineStyles from '../helpers/CombineStyles';
import GlobalStyle from '../helpers/GlobalStyle';
import { redirectToHome } from '../helpers/Redirections';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { withStyles } from '@material-ui/core/styles';
import axios from "axios";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import classNames from 'classnames';
import Chip from "@material-ui/core/Chip";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

const LocalStyle = theme => ({
    button: {
        width: 'auto',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
    },
});

class Login extends Component {

    state = {
        showPassword: false,
        username: '',
        password: '',
        showChip: false,
        inscriptionConfirmed: false,
        token: sessionStorage.getItem('token')
    };

    constructor(props) {
        super(props);
        if (this.state.token) {
            redirectToHome(this.props);
        }
    }

    componentDidMount() {
        this.handleInscriptionConfirmed();
    }

    login = () => {
        this.handleDeleteInscriptionConfirmed();
        this.handleDelete();
        axios.get("http://localhost:3001/users/find/" + this.state.username)
            .then(data => {
                    if (data['data'] === null) {
                        this.setState({showChip: true});
                    } else {
                        if (data['data']['password'] === this.state.password) {
                            sessionStorage.setItem('token', data['data']['_id']);
                            redirectToHome(this.props);
                        } else {
                            this.setState({showChip: true});
                        }
                    }
                }
            );
    };

    handleInscriptionConfirmed = () => {
        if (sessionStorage.getItem('inscription') === 'Account created !') {
            this.setState({inscriptionConfirmed: true});
        }
    };

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    handleDelete = () => {
        this.setState({showChip: false});
    };

    handleDeleteInscriptionConfirmed = () => {
        this.setState({inscriptionConfirmed: false});
        sessionStorage.removeItem('inscription');
    };

    render() {
        const { classes } = this.props;
        return (
            <div className="Login">
                <Grid container direction="column" justify="center" alignItems="center">
                    <Grid item>
                        <Paper className={classes.paperGlobal} elevation={10}>
                            WELCOME
                            <form noValidate autoComplete="off">
                                { this.state.showChip ? <Chip label="Wrong login or password." className={classes.chipError} onDelete={this.handleDelete} /> : null }
                                { this.state.inscriptionConfirmed ? <Chip label="Account created !" className={classes.chipSuccess} onDelete={this.handleDeleteInscriptionConfirmed} /> : null }
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <TextField
                                            id="username"
                                            label="Username"
                                            margin="normal"
                                            value={this.state.username}
                                            onChange={this.handleChange('username')}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <InputLabel htmlFor="adornment-password">Password</InputLabel>
                                        <Input
                                            id="password"
                                            type={this.state.showPassword ? 'text' : 'password'}
                                            value={this.state.password}
                                            onChange={this.handleChange('password')}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="Toggle password visibility"
                                                        onClick={this.handleClickShowPassword}
                                                    >
                                                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </Grid>
                                <br/>
                                <Button variant="contained" color="primary" className={classes.button} onClick={this.login}>
                                    Connection
                                </Button>
                            </form>
                            <hr className={classes.hr}/>
                            <Typography>
                                Psst..! No account ?
                                <br/>
                                <Link to={'/register'} style={{ textDecoration: 'none', color: 'inherit' }}>
                                    <span role="img" aria-labelledby="jsx-a11y/accessible-emoji">👉🏻</span>This way !
                                </Link>
                            </Typography>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
const combinedStyles = CombineStyles(GlobalStyle, LocalStyle);

export default withStyles(combinedStyles)(Login);
