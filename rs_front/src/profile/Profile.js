import React, { Component } from 'react';
import './Profile.css';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CombineStyles from '../helpers/CombineStyles';
import GlobalStyle from '../helpers/GlobalStyle';
import {redirectToLogin, redirectToProfile, redirectToUser} from '../helpers/Redirections';
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import AppBarCustom from '../helpers/AppBarCustom';
import {ToastsContainer, ToastsStore} from "react-toasts";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";

const qs = require('qs');

const header = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};

const LocalStyle = theme => ({
    profileImage: {
        borderRadius: '75px',
        '&:hover': {
            opacity: '0.7',
            borderRadius: '5px',
        },
    },
    button: {
        marginTop: '10px',
        width: '150',
    },
    modalPhoto: {
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 2,
        outline: 'none',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },
    centeredIntoModal: {
        textAlign: 'center',
        marginBottom: '30px',
    },
    uploadButton: {
        width: '300px',
        marginTop: '30px',
    },
    legend: {
        marginBottom: '20px',
    },
    paperLocal: {
        marginTop: '10px',
        padding: theme.spacing.unit,
    },
    textField: {
        width: '100%',
    },
    postButton: {
        marginTop: '10px',
        width: '150px',
    },
    small: {
        lineHeight: '110%',
        fontSize: '80%',
    },
    postAuthor: {
        fontWeight: 'bold',
    },
    postDate: {
        color: '#999999',
    },
    postContent: {
        fontSize: '90%',
        whiteSpace: 'pre-wrap'
    },
    profileMedal: {
        borderRadius: '40px',
    },
    postButtons: {
        marginTop: '15px',
    },
    controlButton: {
        marginLeft: '5px',
        marginRight: '5px',
        minWidth: '15px',
    },
    marginLeftCustom: {
        marginLeft: '8px',
    },
    commentStyle: {
        backgroundColor: '#f6f6f6',
        padding: theme.spacing.unit * 2,
        borderRadius: '5px',
        marginTop: '10px',
    },
    commentButton: {
        width: '34px',
        height: '34px',
        borderRadius: '34px',
    },
    userLink: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'left',
    }
});

class Profile extends Component {

    state = {
        user: [],
        token: sessionStorage.getItem('token'),
        hoverImage: false,
        openModal: false,
        file: null,
        src: null,
        crop: {aspect: 1, width: 50, x: 0, y: 0},
        croppedImageUrl: null,
        step: 1,
        phoneSize: window.innerWidth <= 600,
        openDeleteAlert: false,
        openDeleteAlertComment: false,
        openUpdateAlert: false,
        commentId: -1,
        posts: [],
        currentPost: null,
        currentIndex: -1,
        postContent: "",
        commentContent: "",
        postModify: "",
    };

    constructor(props) {
        super(props);
        if (!this.state.token) {
            redirectToLogin(this.props);
        }
    }

    getCurrentUser = () => {
        axios.get("http://localhost:3001/users/" + this.state.token)
            .then(data => {
                    this.setState({user: data['data']});
                }
            );
    };

    componentDidMount() {
        this.getCurrentUser();
        this.handlePhotoUploaded();
        this.getPosts();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    updateDimensions() {
        this.setState({phoneSize: window.innerWidth <= 600})
    }

    handlePhotoUploaded = () => {
        if (sessionStorage.getItem('photoUpdated') === 'true') {
            ToastsStore.success("Upload worked !");
            sessionStorage.removeItem('photoUpdated');
        } else if (sessionStorage.getItem('photoUpdated') === 'false') {
            ToastsStore.error("Upload didn't work...");
            sessionStorage.removeItem('photoUpdated');
        }
    };

    handleToogleModal = () => {
        this.setState({ openModal: !this.state.openModal });
    };

    onClickHandler = () => {
        const data = new FormData();
        data.append('file', this.state.file);
        data.append('fileName', this.state.user._id);
        axios.post("http://localhost:3001/upload/" + this.state.user._id, data)
            .then(res => {
                let newPhotoName = res['data'];
                axios.put("http://localhost:3001/users/" + this.state.user._id, qs.stringify({photo: newPhotoName}))
                    .then(res => {
                        sessionStorage.setItem('photoUpdated', 'true');
                        window.location.reload();
                    }).catch(res => {
                    sessionStorage.setItem('photoUpdated', 'false');
                });
                this.setState({openModal: false});
                this.setState({src: null});
                this.setState({step: 1});
            });
    };

    onSelectFile = e => {
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result }),
            );
            reader.readAsDataURL(e.target.files[0]);
            this.setState({step: 2})
        }
    };

    onImageLoaded = (image, pixelCrop) => {
        this.imageRef = image;
    };

    onCropComplete = (crop, pixelCrop) => {
        this.makeClientCrop(crop, pixelCrop);
    };

    onCropChange = crop => {
        this.setState({ crop });
    };

    async makeClientCrop(crop, pixelCrop) {
        if (this.imageRef && crop.width && crop.height) {
            let component = this;
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                pixelCrop,
                'temp.png',
            );
            let xhr = new XMLHttpRequest();
            xhr.open('GET', croppedImageUrl, true);
            xhr.responseType = 'blob';
            xhr.onload = function(e) {
                if (this.status === 200) {
                    let blob = this.response;
                    blob.lastModifiedDate = new Date();
                    let file = new File([blob], 'temp.png', {type : 'image/png'});
                    component.setState({file: file});
                }
            };
            xhr.send();
        }
    }

    getCroppedImg(image, pixelCrop, fileName) {
        const canvas = document.createElement('canvas');
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            pixelCrop.x,
            pixelCrop.y,
            pixelCrop.width,
            pixelCrop.height,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height,
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');

        });
    }

    backToStepOne = () => {
        this.setState({ step: 1 });
    };

    post = () => {
        if (this.state.postContent === "") {
            return;
        }
        this.handleDeletePosted();
        let newPost = {
            content: this.state.postContent,
            author: this.state.user._id,
        };
        axios.post("http://localhost:3001/posts", qs.stringify(newPost), header).then(res => {
            sessionStorage.setItem('posted', 'Post created !');
            this.setState({postContent: ""});
            this.getPosts();
            ToastsStore.success("Post successfully created !");
        });
    };

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    formatDate = date => {
        let d = new Date(date);
        let day = d.getDay() < 10 ? '0' + d.getDay() : d.getDay();
        let month = d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth();
        return `${day}/${month}/${d.getFullYear()}`;
    };
    formatHour = date => {
        let d = new Date(date);
        let minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
        return `${d.getHours()}:${minutes}`;
    };

    getPosts = () => {
        axios.get("http://localhost:3001/posts/user/" + sessionStorage.getItem('token'))
            .then(data => {
                    this.setState({posts: data['data'].reverse()});
                }
            );
    };

    likePost = (post, userId, index) => {
        this.setState({currentPost: post});
        axios.put(`http://localhost:3001/posts/like/${post._id}/${userId}`).then(res => {
            this.setState({currentPost: res['data']});
            let posts = this.state.posts;
            posts[index] = this.state.currentPost;
            this.setState({posts: posts});
            ToastsStore.info("Post successfully liked !");
        });
    };

    unlikePost = (post, userId, index) => {
        this.setState({currentPost: post});
        axios.put(`http://localhost:3001/posts/unlike/${post._id}/${userId}`).then(res => {
            this.setState({currentPost: res['data']});
            let posts = this.state.posts;
            posts[index] = this.state.currentPost;
            this.setState({posts: posts});
            ToastsStore.info("Post successfully disliked !");
        });
    };

    handleOpenDeleteAlert = (post, index) => {
        this.setState({ openDeleteAlert: true, currentPost: post, currentIndex: index });
    };

    handleCloseDeleteAlert = () => {
        this.setState({ openDeleteAlert: false });
    };

    deletePost = () => {
        axios.delete(`http://localhost:3001/posts/${this.state.currentPost._id}`).then(res => {
            let posts = this.state.posts;
            posts.splice(this.state.currentIndex, 1);
            this.setState({posts: posts, currentPost: null, currentIndex: -1});
            this.handleCloseDeleteAlert();
            ToastsStore.info("Post successfully deleted !");
        });
    };

    handleOpenUpdateAlert = (post, index) => {
        this.setState({ openUpdateAlert: true, currentPost: post, currentIndex: index, postModify: post.content });
    };

    handleCloseUpdateAlert = () => {
        this.setState({ openUpdateAlert: false });
    };

    updatePost = () => {
        let update = {
            content: this.state.postModify,
        };
        axios.put("http://localhost:3001/posts/" + this.state.currentPost._id, qs.stringify(update), header).then(res => {
            sessionStorage.setItem('posted', 'Post Updated !');
            this.setState({postModify: ""});
            this.getPosts();
            this.handleCloseUpdateAlert();
            ToastsStore.success("Post successfully Updated !");
        });
    };

    comment = post => {
        if (this.state.commentContent === "") {
            return;
        }
        let newComment = {
            post: post._id,
            content: this.state.commentContent,
            author: this.state.user._id,
        };
        axios.post("http://localhost:3001/comments", qs.stringify(newComment), header).then(res => {
            this.setState({commentContent: ""});
            document.getElementById("text-comment-" + post._id).value = "";
            this.getPosts();
            ToastsStore.success("Comment successfully sent !");
        });
    };

    deleteComment = () => {
        axios.delete(`http://localhost:3001/comment/${this.state.commentId}`).then(res => {
            this.setState({ commentId: -1 });
            this.getPosts();
            this.handleCloseDeleteAlertComment();
            ToastsStore.info("Comment successfully deleted !");
        });
    };

    handleOpenDeleteAlertComment = commentId => {
        this.setState({ openDeleteAlertComment: true, commentId: commentId });
    };

    handleCloseDeleteAlertComment = () => {
        this.setState({ openDeleteAlertComment: false });
    };

    toggleHide = postId => {
        let el = document.getElementById("comment-field-" + postId);
        el.style.display = el.style.display === "none" ? "" : "none";
    };

    render() {
        const { crop, src } = this.state;
        const user = this.state.user;
        const posts = this.state.posts;
        const { classes } = this.props;
        return (
            <div className="Profile">
                <AppBarCustom data={this.props} />
                <Grid container justify={"center"}>
                    <Grid item xs={12} sm={10} md={8} lg={7} xl={6}>
                        <Paper className={classNames(classes.paperGlobal, classes.paperLocal)} elevation={10}>
                            <Grid item xs={12} container justify={"center"}>
                                <Grid item xs={6} sm={4} md={3} container justify={"center"}>
                                    <img src={"http://localhost:3001/images/profile/" + user.photo}
                                         className={classes.profileImage}
                                         alt={user.firstName}
                                         width="145"
                                         height="145"
                                    />
                                    <Button variant="contained" color="secondary" className={classes.button} onClick={this.handleToogleModal}>
                                        Change photo
                                    </Button>
                                    <Modal aria-labelledby="modalPhoto" aria-describedby="modelPhotoUpload" open={this.state.openModal} onClose={this.handleToogleModal}>
                                        <div className={classes.modalPhoto}>
                                            <Grid container justify={"center"}>
                                                { this.state.step === 1 ?
                                                    // Step 1 for profile photo - UPLOAD
                                                    <Grid item xs={12} className={classes.centeredIntoModal}>
                                                        <legend className={classes.legend}>Choose a photo</legend>
                                                        <div>
                                                            <input type="file" onChange={this.onSelectFile} />
                                                        </div>
                                                    </Grid>
                                                    :
                                                    // Step 2 for profile photo - CROP
                                                    <Grid item xs={12} className={classes.centeredIntoModal}>
                                                        <legend className={classes.legend}>
                                                            <span className={classes.title} onClick={this.backToStepOne}>❮</span> Crop the photo
                                                        </legend>
                                                        {src && (<ReactCrop src={src} crop={crop} onImageLoaded={this.onImageLoaded}
                                                                            onComplete={this.onCropComplete} onChange={this.onCropChange}
                                                                            style={{ width: '100%' }}/>)}


                                                        <Grid item xs={12} className={classes.centered}>
                                                            <Button variant="contained" color="secondary" className={classes.uploadButton} onClick={this.onClickHandler}>Upload</Button>
                                                        </Grid>
                                                    </Grid>
                                                }
                                            </Grid>
                                        </div>
                                    </Modal>
                                </Grid>
                                <Grid item xs={6} sm={8} md={9} container justify={"center"} direction={"column"} alignItems={"center"}>
                                    <h3>{user.firstName} {user.lastName}</h3>
                                    <span>Username : {user.username}</span>
                                </Grid>
                            </Grid>
                        </Paper>
                        {posts.map((post, index) => (
                            <Paper key={post._id} className={classes.paperLocal} elevation={10}>
                                <Grid item xs={12} container>
                                    <Grid item xs={12} container>
                                        <div className={classNames(classes.userLink, classes.pointer)}
                                             onClick={post.author._id === user._id ?
                                                 () => redirectToProfile(this.props) :
                                                 () => redirectToUser(this.props, post.author._id)}>
                                            <img src={"http://localhost:3001/images/profile/" + post.author.photo}
                                                 alt="Profile Medal"
                                                 width="25px"
                                                 height="25px"
                                                 className={classes.profileMedal}
                                            />
                                            <span className={classes.marginLeftCustom}>
                                                <span className={classNames(classes.small, classes.postAuthor)}>{post.author.firstName} </span>
                                                <span className={classNames(classes.small, classes.postAuthor)}>{post.author.lastName} </span>
                                                <span className={classNames(classes.small, classes.postDate)}>{this.formatDate(post.Created_date)} </span>
                                                <span className={classNames(classes.small, classes.postDate)}>- {this.formatHour(post.Created_date)}</span>
                                            </span>
                                        </div>
                                    </Grid>
                                    <hr className={classes.hr}/>
                                    <Grid item xs={12} container>
                                        <div className={classNames(classes.postContent, classes.fullWidth)}>
                                            {post.content}
                                        </div>
                                        <Grid className={classNames(classes.postButtons, classes.fullWidth, classes.alignToRight)}>
                                            {post.likes.includes(user._id) ? (
                                                <Button size="small"
                                                        variant="contained"
                                                        color="primary"
                                                        className={classes.controlButton}
                                                        onClick={() => this.unlikePost(post, user._id, index)}
                                                >
                                                    {post.likes.length}
                                                    {this.state.phoneSize ?
                                                        <i className="material-icons">thumb_up_alt</i> :
                                                        post.likes.length > 1 ? ' LIKES' : ' LIKE'
                                                    }
                                                </Button>
                                            ) : (
                                                <Button size="small"
                                                        variant="outlined"
                                                        color="primary"
                                                        className={classes.controlButton}
                                                        onClick={() => this.likePost(post, user._id, index)}
                                                >
                                                    {post.likes.length}
                                                    {this.state.phoneSize ?
                                                        <i className="material-icons">thumb_up_alt</i> :
                                                        post.likes.length > 1 ? ' LIKES' : ' LIKE'
                                                    }
                                                </Button>
                                            )}
                                            <Button size="small"
                                                    variant="outlined"
                                                    color="primary"
                                                    className={classes.controlButton}
                                                    onClick={() => this.toggleHide(post._id)}
                                            >
                                                {this.state.phoneSize ? <i className="material-icons">comment</i> : 'COMMENT'}
                                            </Button>
                                            {post.author._id === user._id &&
                                            <Button size="small"
                                                    variant="outlined"
                                                    color="primary"
                                                    className={classes.controlButton}
                                                    onClick={() => this.handleOpenUpdateAlert(post, index)}
                                            >
                                                {this.state.phoneSize ? <i className="material-icons">create</i> : 'UPDATE'}
                                            </Button>
                                            }
                                            {post.author._id === user._id &&
                                            <Button size="small"
                                                    variant="outlined"
                                                    color="secondary"
                                                    className={classes.controlButton}
                                                    onClick={() => this.handleOpenDeleteAlert(post, index)}
                                            >
                                                {this.state.phoneSize ? <i className="material-icons">delete_outline</i> : 'DELETE'}
                                            </Button>
                                            }
                                            <Grid container id={"comment-field-" + post._id} className={classes.commentStyle} style={{display: 'none'}}>
                                                <Grid item xs={11}>
                                                    <TextField
                                                        id={"text-comment-" + post._id}
                                                        placeholder="Your comment"
                                                        multiline={true}
                                                        rows={3}
                                                        className={classNames(classes.textField, "text-comment")}
                                                        onChange={this.handleChange('commentContent')}
                                                    />
                                                </Grid>
                                                <Grid item xs={1}>
                                                    <Button size="small"
                                                            color="primary"
                                                            className={classNames(classes.controlButton, classes.commentButton)}
                                                            onClick={() => this.comment(post)}
                                                    >
                                                        <i className="material-icons">send</i>
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                            {post.comments.map((comment) => (
                                                <Grid item xs={12} container key={comment._id} className={classes.commentStyle}>
                                                    <Grid item xs={12} container>
                                                        <div className={classNames(classes.userLink, classes.pointer)}
                                                             onClick={comment.author._id === user._id ?
                                                                 () => redirectToProfile(this.props) :
                                                                 () => redirectToUser(this.props, comment.author._id)}>
                                                            <img src={"http://localhost:3001/images/profile/" + comment.author.photo}
                                                                 alt="Profile Medal"
                                                                 width="25px"
                                                                 height="25px"
                                                                 className={classes.profileMedal}
                                                            />
                                                            <span className={classes.marginLeftCustom}>
                                                                <span className={classNames(classes.small, classes.postAuthor)}>{comment.author.firstName} </span>
                                                                <span className={classNames(classes.small, classes.postAuthor)}>{comment.author.lastName} </span>
                                                                <span className={classNames(classes.small, classes.postDate)}>{this.formatDate(comment.Created_date)} </span>
                                                                <span className={classNames(classes.small, classes.postDate)}>- {this.formatHour(comment.Created_date)}</span>
                                                            </span>
                                                        </div>
                                                        <Grid item xs={12} container>
                                                            <Grid item xs={comment.author._id === user._id ? 11 : 12} container>
                                                                <div className={classNames(classes.postContent, classes.fullWidth)} style={{textAlign: 'left'}}>
                                                                    {comment.content}
                                                                </div>
                                                            </Grid>
                                                            {comment.author._id === user._id &&
                                                            <Grid item xs={1} container>
                                                                <div className={classNames(classes.postContent, classes.fullWidth)}>
                                                                    <Button size="small"
                                                                            color="primary"
                                                                            className={classNames(classes.controlButton, classes.commentButton)}
                                                                            onClick={() => this.handleOpenDeleteAlertComment(comment._id)}
                                                                    >
                                                                        <i className="material-icons">delete_outline</i>
                                                                    </Button>
                                                                </div>
                                                            </Grid>
                                                            }
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            ))}
                                        </Grid>
                                    </Grid>

                                </Grid>
                            </Paper>
                        ))}
                    </Grid>
                </Grid>
                <Dialog
                    open={this.state.openDeleteAlert}
                    onClose={this.handleCloseDeleteAlert}
                    aria-labelledby="alert-dialog-delete-post"
                    aria-describedby="alert-dialog-delete"
                >
                    <DialogTitle id="alert-dialog-delete-post">{"Are you sure you want to delete this post ?"}</DialogTitle>
                    <DialogContent>
                        <div className={classNames(classes.postContent, classes.fullWidth)}>
                            {this.state.currentPost !== null ? this.state.currentPost.content : ''}
                        </div>
                        <hr className={classes.hr}/>
                        <DialogContentText id="alert-dialog-delete">
                            This action is irreversible, you will never find this post again !
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseDeleteAlert} color="primary">
                            Disagree
                        </Button>
                        <Button onClick={this.deletePost} color="secondary">
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={this.state.openUpdateAlert}
                    onClose={this.handleCloseUpdateAlert}
                    aria-labelledby="alert-dialog-update-post"
                    aria-describedby="alert-dialog-update"
                >
                    <DialogTitle id="alert-dialog-update-post">
                        {"Update the post du " +
                        (this.state.currentPost !== null ?
                            this.formatDate(this.state.currentPost.Created_date) + " à " +  this.formatHour(this.state.currentPost.Created_date) :
                            "")}
                    </DialogTitle>
                    <DialogContent>
                        <TextField
                            placeholder="Talk about something, someone, somewhere... or maybe something important for you"
                            multiline={true}
                            rows={5}
                            className={classes.textField}
                            onChange={this.handleChange('postModify')}
                            value={this.state.postModify}
                        />
                        <hr className={classes.hr}/>
                        <DialogContentText id="alert-dialog-update">
                            Be sure the post is what you want !
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseUpdateAlert} color="primary">
                            Disagree
                        </Button>
                        <Button onClick={this.updatePost} color="secondary">
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={this.state.openDeleteAlertComment}
                    onClose={this.handleCloseDeleteAlertComment}
                    aria-labelledby="alert-dialog-delete-comment"
                    aria-describedby="alert-dialog-delete"
                >
                    <DialogTitle id="alert-dialog-delete-comment">{"Are you sure you want to delete this comment ?"}</DialogTitle>
                    <DialogActions>
                        <Button onClick={this.handleCloseDeleteAlertComment} color="primary">
                            No !
                        </Button>
                        <Button onClick={this.deleteComment} color="secondary">
                            Yes !
                        </Button>
                    </DialogActions>
                </Dialog>
                <ToastsContainer lightBackground store={ToastsStore} />
            </div>
        );
    }
}

const combinedStyles = CombineStyles(GlobalStyle, LocalStyle);

export default withStyles(combinedStyles)(Profile);
