import React, { Component } from 'react';
import './Register.css';
import CombineStyles from '../helpers/CombineStyles';
import GlobalStyle from '../helpers/GlobalStyle';
import { redirectToLogin, redirectToHome } from '../helpers/Redirections';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import { withStyles } from '@material-ui/core/styles';
import axios from "axios";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import classNames from 'classnames';
import Chip from "@material-ui/core/Chip";
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import {Link} from "react-router-dom";

const qs = require('qs');

const LocalStyle = theme => ({
    button: {
        width: 'auto',
    },
    datePicker: {
        width: 250,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
    },
});

class Register extends Component {

    state = {
        showPassword: false,
        showRepeatPassword: false,
        username: '',
        firstName: '',
        lastName: '',
        password: '',
        repeatPassword: '',
        showChip: false,
        showWrongPassword: false,
        usernameUnavailable: false,
        token: sessionStorage.getItem('token')
    };

    constructor(props) {
        super(props);
        if (this.state.token) {
            redirectToHome(this.props);
        }
    }

    createAccount = () => {
        this.handleDeleteWrongPassword();
        this.handleDeleteUsernameUnavailable();
        if (this.state.password === this.state.repeatPassword) {
            axios.get("http://localhost:3001/users/find/" + this.state.username)
                .then(data => {
                        if (data['data'] !== null) {
                            this.setState({usernameUnavailable: true});
                        } else {
                            let newUser = {
                                firstName: this.state.firstName,
                                lastName: this.state.lastName,
                                username: this.state.username,
                                password: this.state.password,
                            };
                            let header = {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            };
                            axios.post("http://localhost:3001/users", qs.stringify(newUser), header).then(res => {
                                sessionStorage.setItem('inscription', 'Account created !');
                                redirectToLogin(this.props);
                            });
                        }
                    }
                );
        } else {
            this.setState({showWrongPassword: true});
        }
    };

    handleChange = prop => event => {
        this.setState({ [prop]: event.target.value });
    };

    handleClickShowPassword = () => {
        this.setState(state => ({ showPassword: !state.showPassword }));
    };

    handleClickShowRepeatPassword = () => {
        this.setState(state => ({ showRepeatPassword: !state.showRepeatPassword }));
    };

    handleDeleteWrongPassword = () => {
        this.setState({showWrongPassword: false});
    };

    handleDeleteUsernameUnavailable = () => {
        this.setState({usernameUnavailable: false});
    };

    handleDateChange = date => {
        this.setState({ selectedDate: date });
    };

    render() {
        const { classes } = this.props;
        return (
            <div className="Register">
                <Grid container direction="column" justify="center" alignItems="center">
                    <Grid item>
                        <Paper className={classes.paperGlobal} elevation={10}>
                            OH ! YOU'RE NEW ?<br/>TELL ME WHO YOU ARE...
                            <form noValidate autoComplete="off">
                                { this.state.showWrongPassword ? <Chip label="Different passwords" className={classes.chipError} onDelete={this.handleDeleteWrongPassword} /> : null }
                                { this.state.usernameUnavailable ? <Chip label="Username unavailable" className={classes.chipError} onDelete={this.handleDeleteUsernameUnavailable} /> : null }
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <TextField
                                            id="firstName"
                                            label="First name"
                                            margin="normal"
                                            value={this.state.firstName}
                                            onChange={this.handleChange('firstName')}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <TextField
                                            id="lastName"
                                            label="Last name"
                                            margin="normal"
                                            value={this.state.lastName}
                                            onChange={this.handleChange('lastName')}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <TextField
                                            id="username"
                                            label="Username"
                                            margin="normal"
                                            value={this.state.username}
                                            onChange={this.handleChange('username')}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <InputLabel htmlFor="adornment-password">Password</InputLabel>
                                        <Input
                                            id="password"
                                            type={this.state.showPassword ? 'text' : 'password'}
                                            value={this.state.password}
                                            onChange={this.handleChange('password')}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="Toggle password visibility"
                                                        onClick={this.handleClickShowPassword}
                                                    >
                                                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl className={classNames(classes.margin, classes.textField)}>
                                        <InputLabel htmlFor="adornment-repeatPassword">Confirm password</InputLabel>
                                        <Input
                                            id="password"
                                            type={this.state.showRepeatPassword ? 'text' : 'password'}
                                            value={this.state.repeatPassword}
                                            onChange={this.handleChange('repeatPassword')}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="Toggle password visibility"
                                                        onClick={this.handleClickShowRepeatPassword}
                                                    >
                                                        {this.state.showRepeatPassword ? <Visibility /> : <VisibilityOff />}
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </Grid>
                                <br/>
                                <Grid item>
                                    <Button variant="contained" color="primary" className={classes.button} onClick={this.createAccount}>
                                        Let's go !
                                    </Button>
                                </Grid>

                                <hr className={classes.hr}/>
                                <Grid item>
                                    <Link className={classes.link} to="/login">
                                        <Button variant="contained" color="secondary" className={classes.button}>
                                            Step back
                                        </Button>
                                    </Link>
                                </Grid>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}
const combinedStyles = CombineStyles(GlobalStyle, LocalStyle);

export default withStyles(combinedStyles)(Register);
